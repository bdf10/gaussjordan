from sage.all import elementary_matrix, identity_matrix, latex, PolynomialRing, ZZ


class GJMove(object):

    def __init__(self, A, i, j):
        self.A = A
        self.i = i
        self.j = j
        self.m = A.nrows()
        self.aij = A[i, j]
        self.col = A.columns()[j]

    def E(self):
        if not self.aij:
            for k, aik in enumerate(self.col):
                if k > self.i and aik:
                    return elementary_matrix(self.m, row1=self.i, row2=k)
        if self.aij != 1:
            return elementary_matrix(self.m, row1=self.i, scale=1/self.aij)
        E = identity_matrix(self.m)
        for k, aik in enumerate(self.col):
            if k != self.i and aik:
                E *= elementary_matrix(self.m, row1=k, row2=self.i, scale=-aik)
        return E

    def latex(self):
        S = PolynomialRing(ZZ, self.m + 1, names='R')
        Ri = S.gens()[self.i + 1]
        if not self.aij:
            for k, aik in enumerate(self.col):
                if k > self.i and aik:
                    Rk = S.gens()[k + 1]
                    return '\\xrightarrow{{{}\\leftrightarrow{}}}'.format(Ri, Rk)
