from functools import partial
from operator import eq
from sage.matrix.matrix_integer_dense import Matrix_integer_dense


class GJMove:

    def __init__(self, A):
        self.A = A





class ZZMatrix(Matrix_integer_dense):

    def __init__(self, parent, value):
        Matrix_integer_dense.__init__(self, parent, value)

    @classmethod
    def from_sage(cls, A):
        return cls(A.parent(), A)

    def greet(self):
        return 'hello world!'
